FROM python:3.8-slim-buster

WORKDIR /app

RUN apt update \
    && apt install -y wget

RUN wget https://github.com/wkhtmltopdf/packaging/releases/download/0.12.6-1/wkhtmltox_0.12.6-1.buster_amd64.deb
RUN apt install ./wkhtmltox_0.12.6-1.buster_amd64.deb -y

RUN pip3 install Flask pdfkit flask-cors

COPY . .

CMD [ "python3", "-m" , "flask", "run", "--host=0.0.0.0"]