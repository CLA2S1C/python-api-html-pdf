import os
import pdfkit
import base64
from flask import Flask, request
from flask_cors import CORS

app = Flask(__name__)
CORS(app)
cors = CORS(app, resource={
    r"/*":{
        "origins":"*"
    }
})

def HtmlToPdf(html):
    options = {
            'page-size': 'Letter',
            'margin-top': '0in',
            'margin-right': '0in',
            'margin-bottom': '0in',
            'margin-left': '0in',
            'encoding': "UTF-8",
            'custom-header': [
                ('Accept-Encoding', 'gzip')
            ],
            'cookie': [
                ('cookie-name1', 'cookie-value1'),
                ('cookie-name2', 'cookie-value2'),
            ],
            'no-outline': None,
            'enable-local-file-access': None
        }
    pdfkit.from_string(html, 'temp.pdf', options=options)
    with open("temp.pdf", "rb") as pdf:
        os.remove("temp.pdf")
        return base64.b64encode(pdf.read())

@app.route("/")
def index():
    return "API-PDF"

@app.route("/", methods=['POST'])
def getPdf():
    req = request.json
    return HtmlToPdf(req['html'])
